import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { ScanComponent } from "~/app/scan/scan.component";

const routes: Routes = [
    { path: "", redirectTo: "/scan", pathMatch: "full" },
    { path: "scan", component: ScanComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
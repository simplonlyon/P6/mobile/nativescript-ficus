import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Product } from '~/app/open-food-facts/product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class OpenFoodFactsService {

  private static readonly URL = "https://fr.openfoodfacts.org/api/v0";
  private static readonly PRODUCT_URL = `${OpenFoodFactsService.URL}/products/`;

  constructor(
    private http: HttpClient
  ) {  }

  getProductByCode(code: string): Observable<Product>{
    let productFilter = map( (rawProduct: any) => rawProduct.product);
    let rawProductObservable = this.http.get<Product>(`${OpenFoodFactsService.PRODUCT_URL}${code}.json`);
    let product = productFilter(rawProductObservable);

    return product
  }
}

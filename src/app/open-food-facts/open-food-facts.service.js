"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var OpenFoodFactsService = /** @class */ (function () {
    function OpenFoodFactsService(http) {
        this.http = http;
    }
    OpenFoodFactsService_1 = OpenFoodFactsService;
    OpenFoodFactsService.prototype.getProductByCode = function (code) {
        var productFilter = operators_1.map(function (rawProduct) { return rawProduct.product; });
        var rawProductObservable = this.http.get("" + OpenFoodFactsService_1.PRODUCT_URL + code + ".json");
        var product = productFilter(rawProductObservable);
        return product;
    };
    OpenFoodFactsService.URL = "https://fr.openfoodfacts.org/api/v0";
    OpenFoodFactsService.PRODUCT_URL = OpenFoodFactsService_1.URL + "/products/";
    OpenFoodFactsService = OpenFoodFactsService_1 = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], OpenFoodFactsService);
    return OpenFoodFactsService;
    var OpenFoodFactsService_1;
}());
exports.OpenFoodFactsService = OpenFoodFactsService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Blbi1mb29kLWZhY3RzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJvcGVuLWZvb2QtZmFjdHMuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBa0Q7QUFDbEQsNENBQXFDO0FBUXJDO0lBS0UsOEJBQ1UsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUNyQixDQUFDOzZCQVBLLG9CQUFvQjtJQVMvQiwrQ0FBZ0IsR0FBaEIsVUFBaUIsSUFBWTtRQUMzQixJQUFJLGFBQWEsR0FBRyxlQUFHLENBQUUsVUFBQyxVQUFlLElBQUssT0FBQSxVQUFVLENBQUMsT0FBTyxFQUFsQixDQUFrQixDQUFDLENBQUM7UUFDbEUsSUFBSSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVSxLQUFHLHNCQUFvQixDQUFDLFdBQVcsR0FBRyxJQUFJLFVBQU8sQ0FBQyxDQUFDO1FBQ3JHLElBQUksT0FBTyxHQUFHLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBRWxELE1BQU0sQ0FBQyxPQUFPLENBQUE7SUFDaEIsQ0FBQztJQWJ1Qix3QkFBRyxHQUFHLHFDQUFxQyxDQUFDO0lBQzVDLGdDQUFXLEdBQU0sc0JBQW9CLENBQUMsR0FBRyxlQUFZLENBQUM7SUFIbkUsb0JBQW9CO1FBSmhDLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQVFnQixpQkFBVTtPQU5mLG9CQUFvQixDQWdCaEM7SUFBRCwyQkFBQzs7Q0FBQSxBQWhCRCxJQWdCQztBQWhCWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgUHJvZHVjdCB9IGZyb20gJ34vYXBwL29wZW4tZm9vZC1mYWN0cy9wcm9kdWN0JztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5cbmV4cG9ydCBjbGFzcyBPcGVuRm9vZEZhY3RzU2VydmljZSB7XG5cbiAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgVVJMID0gXCJodHRwczovL2ZyLm9wZW5mb29kZmFjdHMub3JnL2FwaS92MFwiO1xuICBwcml2YXRlIHN0YXRpYyByZWFkb25seSBQUk9EVUNUX1VSTCA9IGAke09wZW5Gb29kRmFjdHNTZXJ2aWNlLlVSTH0vcHJvZHVjdHMvYDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnRcbiAgKSB7ICB9XG5cbiAgZ2V0UHJvZHVjdEJ5Q29kZShjb2RlOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFByb2R1Y3Q+e1xuICAgIGxldCBwcm9kdWN0RmlsdGVyID0gbWFwKCAocmF3UHJvZHVjdDogYW55KSA9PiByYXdQcm9kdWN0LnByb2R1Y3QpO1xuICAgIGxldCByYXdQcm9kdWN0T2JzZXJ2YWJsZSA9IHRoaXMuaHR0cC5nZXQ8UHJvZHVjdD4oYCR7T3BlbkZvb2RGYWN0c1NlcnZpY2UuUFJPRFVDVF9VUkx9JHtjb2RlfS5qc29uYCk7XG4gICAgbGV0IHByb2R1Y3QgPSBwcm9kdWN0RmlsdGVyKHJhd1Byb2R1Y3RPYnNlcnZhYmxlKTtcblxuICAgIHJldHVybiBwcm9kdWN0XG4gIH1cbn1cbiJdfQ==
export interface Product {
    code: string;
    image_front_url: string;
    allergens_from_ingredients: string;
}
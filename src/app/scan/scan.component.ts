import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from "nativescript-barcodescanner";
import { OpenFoodFactsService } from '~/app/open-food-facts/open-food-facts.service';
import { Product } from '~/app/open-food-facts/product';

@Component({
  selector: 'ns-scan',
  templateUrl: './scan.component.html',
  styleUrls: ['./scan.component.css'],
  moduleId: module.id,
})

export class ScanComponent implements OnInit {

  private static readonly BARCODE_TYPES = "AZTEC, CODE_39, CODE_93, CODE_128, DATA_MATRIX, EAN_8, EAN_13, ITF, PDF_417, QR_CODE, UPC_E, CODABAR, MAXICODE, RSS_14, UPC_A";

  constructor(
    private barcodeScanner: BarcodeScanner,
    private offService: OpenFoodFactsService
    ) { }

  ngOnInit() {
  }

  product: Product;

  public scanTapped(): void {
    this.barcodeScanner.hasCameraPermission()
      .then(granted => granted ? this.scan() : console.log("Permission denied"))
      .catch(() => {
        this.barcodeScanner.requestCameraPermission()
          .then(() => this.scan());
      });
  }

  private scan() {
    this.barcodeScanner.scan({
      formats: ScanComponent.BARCODE_TYPES,
      beepOnScan: true,
      reportDuplicates: true,
      preferFrontCamera: false,
      showTorchButton: true
    })
      .then(result => {
        console.log(JSON.stringify(result));
        this.getProductByCode(result.text);
      })
      .catch(error => console.log(error));
  }

  private getProductByCode(code: string): void {
    this.offService.getProductByCode(code).subscribe(
      (result) => {this.product = result},
      (error) => {console.log(error)}
    )
  }
}

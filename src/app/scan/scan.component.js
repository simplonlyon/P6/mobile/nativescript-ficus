"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_barcodescanner_1 = require("nativescript-barcodescanner");
var open_food_facts_service_1 = require("~/app/open-food-facts/open-food-facts.service");
var ScanComponent = /** @class */ (function () {
    function ScanComponent(barcodeScanner, offService) {
        this.barcodeScanner = barcodeScanner;
        this.offService = offService;
    }
    ScanComponent_1 = ScanComponent;
    ScanComponent.prototype.ngOnInit = function () {
    };
    ScanComponent.prototype.scanTapped = function () {
        var _this = this;
        this.barcodeScanner.hasCameraPermission()
            .then(function (granted) { return granted ? _this.scan() : console.log("Permission denied"); })
            .catch(function () {
            _this.barcodeScanner.requestCameraPermission()
                .then(function () { return _this.scan(); });
        });
    };
    ScanComponent.prototype.scan = function () {
        var _this = this;
        this.barcodeScanner.scan({
            formats: ScanComponent_1.BARCODE_TYPES,
            beepOnScan: true,
            reportDuplicates: true,
            preferFrontCamera: false,
            showTorchButton: true
        })
            .then(function (result) {
            console.log(JSON.stringify(result));
            _this.getProductByCode(result.text);
        })
            .catch(function (error) { return console.log(error); });
    };
    ScanComponent.prototype.getProductByCode = function (code) {
        var _this = this;
        this.offService.getProductByCode(code).subscribe(function (result) { _this.product = result; }, function (error) { console.log(error); });
    };
    ScanComponent.BARCODE_TYPES = "AZTEC, CODE_39, CODE_93, CODE_128, DATA_MATRIX, EAN_8, EAN_13, ITF, PDF_417, QR_CODE, UPC_E, CODABAR, MAXICODE, RSS_14, UPC_A";
    ScanComponent = ScanComponent_1 = __decorate([
        core_1.Component({
            selector: 'ns-scan',
            templateUrl: './scan.component.html',
            styleUrls: ['./scan.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [nativescript_barcodescanner_1.BarcodeScanner,
            open_food_facts_service_1.OpenFoodFactsService])
    ], ScanComponent);
    return ScanComponent;
    var ScanComponent_1;
}());
exports.ScanComponent = ScanComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nhbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzY2FuLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCwyRUFBNkQ7QUFDN0QseUZBQXFGO0FBVXJGO0lBSUUsdUJBQ1UsY0FBOEIsRUFDOUIsVUFBZ0M7UUFEaEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQXNCO0lBQ3BDLENBQUM7c0JBUEksYUFBYTtJQVN4QixnQ0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUlNLGtDQUFVLEdBQWpCO1FBQUEsaUJBT0M7UUFOQyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixFQUFFO2FBQ3RDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEVBQXhELENBQXdELENBQUM7YUFDekUsS0FBSyxDQUFDO1lBQ0wsS0FBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsRUFBRTtpQkFDMUMsSUFBSSxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsSUFBSSxFQUFFLEVBQVgsQ0FBVyxDQUFDLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8sNEJBQUksR0FBWjtRQUFBLGlCQWFDO1FBWkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7WUFDdkIsT0FBTyxFQUFFLGVBQWEsQ0FBQyxhQUFhO1lBQ3BDLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsaUJBQWlCLEVBQUUsS0FBSztZQUN4QixlQUFlLEVBQUUsSUFBSTtTQUN0QixDQUFDO2FBQ0MsSUFBSSxDQUFDLFVBQUEsTUFBTTtZQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsQ0FBQyxDQUFDO2FBQ0QsS0FBSyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFTyx3Q0FBZ0IsR0FBeEIsVUFBeUIsSUFBWTtRQUFyQyxpQkFLQztRQUpDLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUM5QyxVQUFDLE1BQU0sSUFBTSxLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQSxDQUFBLENBQUMsRUFDbkMsVUFBQyxLQUFLLElBQU0sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFBLENBQUMsQ0FDaEMsQ0FBQTtJQUNILENBQUM7SUF6Q3VCLDJCQUFhLEdBQUcsK0hBQStILENBQUM7SUFGN0osYUFBYTtRQVB6QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFNBQVM7WUFDbkIsV0FBVyxFQUFFLHVCQUF1QjtZQUNwQyxTQUFTLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztZQUNuQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzt5Q0FPMEIsNENBQWM7WUFDbEIsOENBQW9CO09BTi9CLGFBQWEsQ0E0Q3pCO0lBQUQsb0JBQUM7O0NBQUEsQUE1Q0QsSUE0Q0M7QUE1Q1ksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWJhcmNvZGVzY2FubmVyXCI7XG5pbXBvcnQgeyBPcGVuRm9vZEZhY3RzU2VydmljZSB9IGZyb20gJ34vYXBwL29wZW4tZm9vZC1mYWN0cy9vcGVuLWZvb2QtZmFjdHMuc2VydmljZSc7XG5pbXBvcnQgeyBQcm9kdWN0IH0gZnJvbSAnfi9hcHAvb3Blbi1mb29kLWZhY3RzL3Byb2R1Y3QnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1zY2FuJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NjYW4uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zY2FuLmNvbXBvbmVudC5jc3MnXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5cbmV4cG9ydCBjbGFzcyBTY2FuQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBwcml2YXRlIHN0YXRpYyByZWFkb25seSBCQVJDT0RFX1RZUEVTID0gXCJBWlRFQywgQ09ERV8zOSwgQ09ERV85MywgQ09ERV8xMjgsIERBVEFfTUFUUklYLCBFQU5fOCwgRUFOXzEzLCBJVEYsIFBERl80MTcsIFFSX0NPREUsIFVQQ19FLCBDT0RBQkFSLCBNQVhJQ09ERSwgUlNTXzE0LCBVUENfQVwiO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgYmFyY29kZVNjYW5uZXI6IEJhcmNvZGVTY2FubmVyLFxuICAgIHByaXZhdGUgb2ZmU2VydmljZTogT3BlbkZvb2RGYWN0c1NlcnZpY2VcbiAgICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgcHJvZHVjdDogUHJvZHVjdDtcblxuICBwdWJsaWMgc2NhblRhcHBlZCgpOiB2b2lkIHtcbiAgICB0aGlzLmJhcmNvZGVTY2FubmVyLmhhc0NhbWVyYVBlcm1pc3Npb24oKVxuICAgICAgLnRoZW4oZ3JhbnRlZCA9PiBncmFudGVkID8gdGhpcy5zY2FuKCkgOiBjb25zb2xlLmxvZyhcIlBlcm1pc3Npb24gZGVuaWVkXCIpKVxuICAgICAgLmNhdGNoKCgpID0+IHtcbiAgICAgICAgdGhpcy5iYXJjb2RlU2Nhbm5lci5yZXF1ZXN0Q2FtZXJhUGVybWlzc2lvbigpXG4gICAgICAgICAgLnRoZW4oKCkgPT4gdGhpcy5zY2FuKCkpO1xuICAgICAgfSk7XG4gIH1cblxuICBwcml2YXRlIHNjYW4oKSB7XG4gICAgdGhpcy5iYXJjb2RlU2Nhbm5lci5zY2FuKHtcbiAgICAgIGZvcm1hdHM6IFNjYW5Db21wb25lbnQuQkFSQ09ERV9UWVBFUyxcbiAgICAgIGJlZXBPblNjYW46IHRydWUsXG4gICAgICByZXBvcnREdXBsaWNhdGVzOiB0cnVlLFxuICAgICAgcHJlZmVyRnJvbnRDYW1lcmE6IGZhbHNlLFxuICAgICAgc2hvd1RvcmNoQnV0dG9uOiB0cnVlXG4gICAgfSlcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KHJlc3VsdCkpO1xuICAgICAgICB0aGlzLmdldFByb2R1Y3RCeUNvZGUocmVzdWx0LnRleHQpO1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRQcm9kdWN0QnlDb2RlKGNvZGU6IHN0cmluZyk6IHZvaWQge1xuICAgIHRoaXMub2ZmU2VydmljZS5nZXRQcm9kdWN0QnlDb2RlKGNvZGUpLnN1YnNjcmliZShcbiAgICAgIChyZXN1bHQpID0+IHt0aGlzLnByb2R1Y3QgPSByZXN1bHR9LFxuICAgICAgKGVycm9yKSA9PiB7Y29uc29sZS5sb2coZXJyb3IpfVxuICAgIClcbiAgfVxufVxuIl19